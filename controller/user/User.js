const User = require("../../modules/user");

const user = async (req, res) => {
    try {
        const user = await User.findById(req.params.id);
        if (!user) {
            return res.send(`User not Found`);
        } else {
            const { password, _id, __v, ...others } = user._doc;
            res.status(200).send({ ...others });
        }
    } catch (err) {
        res.status(500).json(err);
    }
};

module.exports = { user };
