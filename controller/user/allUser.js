const User = require("../../modules/user");

const allUser = async (req, res) => {
    try {
        const user = await User.find();
        res.status(200).json(user);
    } catch (err) {
        res.status(500).json(err);
    }
};

module.exports = { allUser };
