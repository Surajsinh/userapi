const CryptoJS = require("crypto-js");
require("dotenv").config();
const User = require("../../modules/user");

const login = async (req, res) => {
    try {
        const user = await User.findOne({
            email: req.body.email,
        });
        if (!user) {
            return res.status(401).json("User not found");
        } else {
            const hashPass = CryptoJS.AES.decrypt(
                user.password,
                process.env.PASS_SEC
            );
            const originalPassword = hashPass.toString(CryptoJS.enc.Utf8);
            const inputPassword = req.body.password;
            if (originalPassword != inputPassword) {
                return res.status(401).json("Wrong Password");
            } else {
                const { password, _id, __v, isAdmin, ...others } = user._doc;
                res.status(200).json({ ...others });
            }
        }
    } catch (error) {
        res.send(error.message);
    }
    //res.send("LoginPage");
};

module.exports = { login };
