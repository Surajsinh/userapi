const CryptoJS = require("crypto-js");
const multer = require("multer");
require("dotenv").config();
const User = require("../../modules/user");

const register = async (req, res) => {
    try {
        const newUser = new User({
            name: req.body.name,
            email: req.body.email,
            phoneNo: req.body.phoneNo,
            address: {
                address: req.body.address,
                city: req.body.city,
                state: req.body.state,
                country: req.body.country,
                zipcode: req.body.zipcode,
            },
            image: req.file.path,
            //password: req.body.name + "@S",
            password: await CryptoJS.AES.encrypt(
                req.body.name + "@S",
                process.env.PASS_SEC
            ).toString(),
            gender: req.body.gender,
            dob: req.body.dob,
            category: req.body.category,
        });
        const Users = await newUser.save();
        const { password, _id, __v, isAdmin, ...others } = newUser._doc;
        res.status(200).send({ ...others });
    } catch (e) {
        res.status(500).send(e.message);
    }
};

const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, "./Uploads/");
    },
    filename: function (req, file, cb) {
        cb(null, new Date().toISOString() + file.originalname);
    },
});

const fileFilter = (req, file, cb) => {
    //reject file
    if (file.mimetype === "image/jpeg" || file.mimetype === "image/png") {
        cd(null, true);
    } else {
        cd(null, false);
    }
};

const upload = multer({
    storage: storage,
    limits: { fileSize: 1024 * 1024 * 5 },
    fileFilter: fileFilter,
});

module.exports = { register, upload };
