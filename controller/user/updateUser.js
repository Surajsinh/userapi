const User = require("../../modules/user");

const updateUser = async (req, res) => {
    try {
        const user = await User.findById(req.params.id);
        if (!user) {
            return res.send(`User not Found`);
        } else {
            if (req.body.password) {
                req.body.password = await CryptoJS.AES.encrypt(
                    req.body.password,
                    process.env.PASS_SEC
                ).toString();
            }
            try {
                const updatedUser = await User.findByIdAndUpdate(
                    req.params.id,
                    {
                        $set: req.body,
                    },
                    { new: true }
                );
                res.status(200).send(updatedUser);
            } catch (err) {
                res.status(500).send(err);
            }
        }
    } catch (err) {
        res.status(500).json(err);
    }
};

module.exports = { updateUser };
