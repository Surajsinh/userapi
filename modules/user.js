const mongoose = require("mongoose");

const userSchema = new mongoose.Schema(
    {
        isAdmin: { type: Boolean, default: false },
        name: {
            type: String,
            required: true,
        },
        email: {
            type: String,
            required: true,
            unique: true,
            lowercase: true,
        },
        address: {
            address: { type: String },
            city: { type: String },
            state: { type: String },
            country: { type: String, default: "India" },
            zipcode: { type: Number, default: 000000 },
        },
        phoneNo: {
            type: Number,
            required: true,
            unique: true,
        },
        image: {
            type: String,
        },
        gender: {
            type: String,
            required: true,
            enum: "male" || "female",
        },
        dob: { type: Date },
        category: { type: String },
        password: {
            type: String,
            required: true,
        },
    },
    { timestamps: true }
);

const User = new mongoose.model("User", userSchema);

module.exports = User;
