const router = require("express").Router();
const { login } = require("../controller/user/login");
const { register } = require("../controller/user/register");
const { allUser } = require("../controller/user/allUser");
const { user } = require("../controller/user/User");
const { updateUser } = require("../controller/user/updateUser");
const { deleteUser } = require("../controller/user/deleteUser");
const multer = require("multer");
const { upload } = require("../controller/user/register");


//routes
router.post("/Register", upload.single("image"), register);
router.post("/Login", login);
router.get("/AllUser", allUser);
router.get("/User/:id", user);
router.put("/User/:id", updateUser);
router.delete("/User/:id", deleteUser);

module.exports = router;
