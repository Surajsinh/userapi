const express = require("express");
const app = express();
require("../db/con");
require("dotenv").config();
const userRoute = require("../routes/user");



app.use(express.json());
app.use(express.urlencoded({ extended: false }));

//routes
app.use("/users", userRoute);


app.listen(process.env.PORT || 5000, () => {
    console.log("Server is running!");
});